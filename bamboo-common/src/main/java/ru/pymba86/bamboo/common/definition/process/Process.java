package ru.pymba86.bamboo.common.definition.process;

import ru.pymba86.bamboo.common.io.Resource;

import java.util.Map;

/**
 *  Процесс представляет собой одну модульную часть бизнес-логики, которая может быть выполнена.
 *  Могут существовать различные типы процессов.
 */
public interface Process {

    /**
     * Получить уник. ид процесса
     *
     * @return уник. ид
     */
    String getId();

    /**
     * Получить имя процесса
     *
     * @return имя
     */
    String getName();

    /**
     * Получить тип процесса
     * @return тип
     */
    String getType();

    /**
     * Получить метаданные, которые связаны с node(узлом)
     * @return метаданные node(узла)
     */
    Map<String,Object> getMetadata();

    /**
     * Получить ресурс(вх. источник) процесса
     * @return ресурс
     */
    Resource getResource();

    /**
     * Установить источник ресурса для процесса
     * @param resource источник ресурса
     */
    void setResource(Resource resource);
}
