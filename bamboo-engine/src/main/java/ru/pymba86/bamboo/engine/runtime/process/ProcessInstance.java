package ru.pymba86.bamboo.engine.runtime.process;

import ru.pymba86.bamboo.engine.runtime.Execution;

public interface ProcessInstance extends Execution {

    /**
     * Ключ процесса
     **/
    String getKey();

    /**
     * Идентификатор процесса
     **/
    String getProcessDefinitionId();

    /**
     * Возвращает последние переменные экземпляра процесса.
     */
   // VariableMap getVariables();
}
