package ru.pymba86.bamboo.engine.runtime;

public interface Execution {

    /**
     * Уникальный идентификатор
     **/
    String getId();

    /**
     * Флаг приостановление выполения
     **/
    boolean isSuspended();

    /**
     * Флаг заверщения выполения
     **/
    boolean isEnded();

    /**
     * Получить ид процесса, в котором был запущем execution
     **/
    String getProcessInstanceId();


}
