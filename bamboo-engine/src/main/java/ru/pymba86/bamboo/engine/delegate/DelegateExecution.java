package ru.pymba86.bamboo.engine.delegate;

public interface DelegateExecution {

    /**
     * Получить номер родителя
     * Если его нет, то  execution представляет экземпляр процесса
     **/
    String getParentId();
}
