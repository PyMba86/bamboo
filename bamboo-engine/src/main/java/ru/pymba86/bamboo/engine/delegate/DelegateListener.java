package ru.pymba86.bamboo.engine.delegate;

public interface DelegateListener<T extends BaseDelegateExecution> {
    void notify(T instance) throws Exception;
}
