package ru.pymba86.bamboo.engine.delegate;

public interface DelegateTask {

    /**
     * Номер задания в базе данных
     **/
    String getId();

    /**
     * Название задания
     **/
    String getName();

    /**
     * Присвоить новое название задания
     **/
    void setName(String name);

    /**
     * Получить описания задания
     **/
    String getDescription();

    /**
     * Присвоить описание задания
     **/
    String setDescription(String description);

}
