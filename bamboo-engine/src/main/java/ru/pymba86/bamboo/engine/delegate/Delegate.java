package ru.pymba86.bamboo.engine.delegate;

public interface Delegate {
    void execute(DelegateExecution execution) throws Exception;
}
