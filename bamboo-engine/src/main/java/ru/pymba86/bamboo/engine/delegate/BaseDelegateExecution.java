package ru.pymba86.bamboo.engine.delegate;

public interface BaseDelegateExecution {

    /**
     * Получить уникальный номер execution
     **/
    String getId();

    /**
     * Получить название события
     **/
    String getEventName();

    /**
     * Получить ключ процесса, в котором был запущем execution
     **/
    String getProcessInstanceKey();

}
