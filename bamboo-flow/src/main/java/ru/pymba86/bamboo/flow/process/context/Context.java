package ru.pymba86.bamboo.flow.process.context;

/**
 * Контекст процесса
 */
public interface Context {

    /**
     * Тип контекста
     *
     * @return тип
     */
    String getType();

    /**
     * Получить Ун. ид
     *
     * @return ун. ид
     */
    long getId();

    /**
     * Установить ун. ид
     *
     * @param id ун. ид
     */
    void setId(long id);

    /**
     * Разрешить контекст
     *
     * @param param параметр
     * @return контекст
     */
    Context resolve(Object param);

}
