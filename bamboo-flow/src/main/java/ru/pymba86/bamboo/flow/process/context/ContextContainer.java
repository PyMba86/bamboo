package ru.pymba86.bamboo.flow.process.context;

import java.util.ArrayList;
import java.util.List;

public interface ContextContainer {

    /**
     * Вернуть список контекстов по типу
     *
     * @param contextType тип контекста
     * @return контексты принадлежащие к типу
     */
    public List<Context> getContextList(String contextType);

    /**
     * Добавить контекст
     *
     * @param context контекст
     */
    public void addContext(Context context);

    /**
     * Получить контекст
     *
     * @param contextType тип контекста
     * @param id          уник. ид
     * @return контекст
     */
    public Context getContext(String contextType, long id);

    /**
     * Добавить контекст по умолчанию
     *
     * @param context контекст
     */
    public void setDefaultContext(Context context);

    /**
     * Получить контекст по умолчанию
     *
     * @param contextType тип контеста
     * @return контекст
     */
    public Context getDefaultContext(String contextType);
}
