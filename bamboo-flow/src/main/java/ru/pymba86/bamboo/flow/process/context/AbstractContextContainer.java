package ru.pymba86.bamboo.flow.process.context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Контейнер контекстов
 */
public abstract class AbstractContextContainer implements ContextContainer {

    private Map<String, List<Context>> subContexts = new HashMap<>();
    private Map<String, Context> defaultContexts = new HashMap<>();
    private long lastContextId;


    /**
     * Вернуть список контекстов по типу
     *
     * @param contextType тип контекста
     * @return контексты принадлежащие к типу
     */
    public List<Context> getContextList(String contextType) {
        return this.subContexts.get(contextType);
    }

    ;

    /**
     * Добавить контекст
     *
     * @param context контекст
     */
    public void addContext(Context context) {
        List<Context> list = this.subContexts.computeIfAbsent(context.getType(), k -> new ArrayList<>());
        if (!list.contains(context)) {
            list.add(context);
            context.setId(++lastContextId);
        }
    }

    /**
     * Получить контекст
     *
     * @param contextType тип контекста
     * @param id          уник. ид
     * @return контекст
     */
    public Context getContext(String contextType, long id) {
        return this.subContexts.get(contextType).stream()
                .filter(context -> context.getId() == id)
                .findFirst()
                .orElse(null);
    }

    /**
     * Добавить контекст по умолчанию
     *
     * @param context контекст
     */
    public void setDefaultContext(Context context) {
        this.defaultContexts.put(context.getType(), context);
    }

    /**
     * Получить контекст по умолчанию
     *
     * @param contextType тип контеста
     * @return контекст
     */
    public Context getDefaultContext(String contextType) {
        return defaultContexts.get(contextType);
    }
}
