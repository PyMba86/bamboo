package ru.pymba86.bamboo.flow.process.context;

public abstract class AbstractContext implements Context {

    private long id;
    private ContextContainer contextContainer;

    /**
     * Получить ид контекста
     *
     * @return ид
     */
    public long getId() {
        return this.id;
    }

    /**
     * Установить ид контекста
     *
     * @param id ун. ид
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Получить контейнер контекстов
     *
     * @return контейнер контекстов
     */
    public ContextContainer getContextContainer() {
        return contextContainer;
    }

    /**
     * Установить новый контейнер контекстов
     *
     * @param contextContainer контейнер контекстов
     */
    public void setContextContainer(ContextContainer contextContainer) {
        this.contextContainer = contextContainer;
    }
}
