package ru.pymba86.bamboo.flow.process.context;

public interface ContextResolver {

    /**
     * Разрешить контекст
     *
     * @param param параметр
     * @return контекст
     */
    Context resolve(String contextId, Object param);
}
