package ru.pymba86.bamboo.flow.process;

import ru.pymba86.bamboo.common.io.Resource;
import ru.pymba86.bamboo.flow.process.context.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Process implements ru.pymba86.bamboo.common.definition.process.Process, ContextContainer, ContextResolver, Serializable {

    private String id;
    private String name;
    private String type;
    private Resource resource;
    private Map<String, Object> metaData = new HashMap<>();
    private ContextContainer contextContainer;

    public Process(ContextContainer contextContainer) {
        this.contextContainer = contextContainer;
    }

    /**
     * Установить ид процесса
     *
     * @param id ид процесса
     */

    void setId(String id) {
        this.id = id;
    }

    /**
     * Получить ид процесса
     *
     * @return ид процесса
     */
    public String getId() {
        return this.id;
    }

    /**
     * Установить название процесса
     *
     * @param name название процесса
     */
    void setName(String name) {
        this.name = name;
    }

    /**
     * Получить название процессп
     *
     * @return название процессп
     */
    public String getName() {
        return this.name;
    }

    /**
     * Установить тип процесса
     *
     * @param type тип процесса
     */
    void setType(String type) {
        this.type = type;
    }

    /**
     * Получить тип процесса
     *
     * @return тип процесса
     */
    public String getType() {
        return this.type;
    }

    /**
     * Добавить метаданные к процессу
     *
     * @param name  название данных
     * @param value значение
     */
    void setMetadata(String name, Object value) {
        this.metaData.put(name, value);
    }

    /**
     * Получить метаданные
     *
     * @return метаданные процесса
     */
    public Map<String, Object> getMetadata() {
        return this.metaData;
    }

    /**
     * Получить метаданные по имени
     *
     * @return метаданные процесса
     */
    public Object getMetadata(String name) {
        return this.metaData.get(name);
    }

    /**
     * Получить ресурс процесса
     *
     * @return ресурс
     */
    public Resource getResource() {
        return this.resource;
    }

    /**
     * Установить новый ресурс
     *
     * @param resource источник ресурса
     */
    public void setResource(Resource resource) {
        this.resource = resource;
    }

    /**
     * Разрешение на параметр в процессе
     *
     * @param param параметр
     * @return контекст
     */
    public Context resolve(String contextId, Object param) {
        Context context = getDefaultContext(contextId);
        if (context != null) {
            context = context.resolve(param);
            if (context != null) {
                return context;
            }
        }
        return null;
    }


    @Override
    public List<Context> getContextList(String contextType) {
        return this.contextContainer.getContextList(contextType);
    }

    @Override
    public void addContext(Context context) {
        this.contextContainer.addContext(context);
        ((AbstractContext) context).setContextContainer(this);
    }

    @Override
    public Context getContext(String contextType, long id) {
        return this.contextContainer.getContext(contextType, id);
    }

    @Override
    public void setDefaultContext(Context context) {
        this.contextContainer.setDefaultContext(context);
        ((AbstractContext) context).setContextContainer(this);
    }

    @Override
    public Context getDefaultContext(String contextType) {
        return this.contextContainer.getDefaultContext(contextType);
    }
}
